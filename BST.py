#Antonio Silva Paucar CIS313 Lab2

from Node import Node

class BST(object):
    def __init__(self):##
        self.__root = None

    def getRoot(self):##
        # Private Method, can only be used inside of BST.
        return self.__root#return the root

    def __findNode(self, data):
        # Private Method, can only be used inside of BST.
        # Search tree for a node whose data field is equal to data.
        # Return the Node object
        if self.__root is None:#if empty return null
            return None

        if self.__root.getData == data:
            return self.__root

        temp = self.__root

        while temp is not None:
            if temp.getData() == data:
                return temp
            else:
                if data<temp.getData():
                    temp = temp.getLeftChild()
                else:
                    temp = temp.getRightChild()

        return temp

    def contains(self, data):###
        # return True of node containing data is present in the tree.
        # otherwise, return False.
        if self.__root is  None:
            return False

        if self.__findNode(data) != None:
            return True
        else:
            return False


    def insert(self, data):
        # Find the right spot in the tree for the new node
        # Make sure to check if anything is in the tree
        # Hint: if a node n is null, calling n.getData() will cause an error

        if self.__root is None:
            self.__root = Node(data)
            return

        temp = self.__root

        while True:
            if data < temp.getData():
                #print("Left")
                if temp.getLeftChild() is not None:
                    temp = temp.getLeftChild();
                else:
                    nuevo = Node(data)
                    nuevo.setParent(temp)
                    temp.setLeftChild(nuevo)
                    #print(nuevo.getData())
                    break

            else:
                #print("right")
                if temp.getRightChild() is not None:
                    temp = temp.getRightChild()
                else:
                    nuevo = Node(data)
                    nuevo.setParent(temp)
                    temp.setRightChild(nuevo)
                    #print(nuevo.getData())

                    break




    def delete(self, data):
        # Find the node to delete.
        # If the value specified by delete does not exist in the tree, then don't change the tree.
        # If you find the node and ...
        #  a) The node has no childre/Users/antonio/Desktop/lab2/BST.py/Users/antonio/Desktop/lab2/inSample.txt/Users/antonio/Desktop/lab2/lab2.py/Users/antonio/Desktop/lab2/Node.pyn, just set it's parent's pointer to Null.
        #  b) The node has one child, make the nodes parent point to its child.
        #  c) The node has two children, replace it with its successor, and remove
        #       successor from its previous location.
        # Recall: The successor of a node is the left-most node in the node's right subtree.
        # Hint: you may want to write a new method, findSuccessor() to find the successor when there are two children
        temp = self.__findNode(data)

        if temp is None:
            return

        if temp is not None:

            # print(temp.getData())
            # print(temp.getLeftChild().getData())
            #   print(temp.getRightChild().getData())

            parent = temp.getParent()

            if temp.getLeftChild() is None and temp.getRightChild() is None:#when has no children
                print("No children")
                if parent.getRightChild() is temp:
                    parent.setRightChild(None)
                else:
                    parent.setLeftChild(None)

            elif temp.getLeftChild() is not None and temp.getRightChild() is None:#one child, left
                print("right children")
                if temp is self.__root:
                    self.__root = temp.getLeftChild()
                else:
                    parent.setLeftChild(temp.getLeftChild)

            elif temp.getLeftChild() is None and temp.getRightChild is not None:
                print("left children")
                if temp is self.__root:
                    self.__root = temp.getRightChild()
                else:
                    parent.setRightChild(temp.getRightChild())


            elif temp.getLeftChild() is not None and temp.getRightChild() is not None:

                sucesor = self.__findSuccessor(temp)

                temp.setData(sucesor.getData())
                padre = sucesor.getParent()

                if padre.getLeftChild().getData()  is sucesor.getData():
                    padre.setLeftChild(None)
                else:
                    padre.setRightChild(None)



    def __findSuccessor(self, aNode):

        if aNode.getRightChild() is not None:
            return self.__findMin(aNode.getRightChild())

        temp = aNode.getParent()

        while temp is not None and aNode == temp.getRightChild():
            aNode = temp
            temp = temp.getParent()

        return temp


    def __findMin(self, node):
        temp = node
        while temp.getLeftChild() is not None:
            temp = temp.getLeftChild()

        return temp



    def traverse(self, order, top):
        # traverse the tree by printing out the node data for all node in a specified order

        if top is not None:
            if order == "preorder":
                # your code here, remove pass
                self.__preOrder(top)
                print
                return

            elif order == "inorder":
                # your code here, remove pass
                self.__inOrder(top)
                print
                return

            elif order == "postorder":
                # your code here, remove pass
                self.__postOrder(top)
                print
                return

            else:
                print("Error, order {} undefined".format(order))


    def __inOrder(self, top):

        if top is None:
            return

        self.__inOrder(top.getLeftChild())
        #print("{}".format(top.getData()), end=" ") python 3
        print (top.getData())
        self.__inOrder(top.getRightChild())


    def __postOrder(self, top):

        if top is None:
            return

        self.__postOrder(top.getLeftChild())
        self.__postOrder(top.getRightChild())
        #print ("{}".format(top.getData()), end=" ") python 3
        print (top.getData())



    def __preOrder(self, top):

        if top is None:

            return

        #print("{}".format(top.getData()), end=" ")  python3
        print (top.getData())
        self.__preOrder(top.getLeftChild())
        self.__preOrder(top.getRightChild())
