#Antonio Silva Paucar CIS313 Lab2

from sys import argv
from BST import BST
import random


def main(argv):
    # Loop over input file (filename passed via argv).
    # Split each line into a task and number (if one exists) 
    #   hint: use the split method for strings https://docs.python.org/2/library/string.html#string.split
    # Perform the function corresponding to the specified task
    # i.e., insert, delete, inorder, preorder, postorder
    # Close the file when you're done.
    nuevo  = BST()
    
    input_file = argv[1]
    with open(input_file, 'r') as file_ob:

        for line in file_ob:
            
            line = line.split()
            line[0] = line[0].strip()

            if line[0] == "insert":
                line[1] = line[1].strip()
                nuevo.insert(line[1])
            
            if line[0] == "delete":
                line[1] = line[1].strip()
                nuevo.delete(line[1])
            
            if line[0] == "inorder":
                nuevo.traverse("inorder", nuevo.getRoot())
            
            if line[0] == "preorder":
                nuevo.traverse("preorder", nuevo.getRoot())
            
            if line[0] == "postorder":
                nuevo.traverse("postorder", nuevo.getRoot())








            



    
    


if __name__ == "__main__":
    main(argv)

